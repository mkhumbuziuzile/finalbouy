import time
def encode(sequence):
    count = 1
    result = []
    for x,item in enumerate(sequence):
        if x == 0:
            continue
        elif item == sequence[x - 1]:
            count += 1
        else:
            result.append((sequence[x - 1], count))
            count = 1
    result.append((sequence[len(sequence) - 1], count))
    return result
    
def decode(sequence):
    result = []
    for item in sequence:
        result.append(item[0] * item[1])
    return "".join(result)

def formatOutput(sequence):
    result = []
    for item in sequence:
        if (item[1] == 1):
            result.append(item[0])
        else:
            result.append(str(item[1]) + item[0])
    return "".join(result)

def main():
    start = time.perf_counter()
    print("Compressing the data.....\n")
    #Commented out things used during demonstation
    #print("Run Length Encoding and Decoding")
    #print("==============================================")
    #h = int(input("Enter 1 if you want to enter in command window, 2 if you are using some file:"))
    h=2 #set automatic file compresion
    if h == 1:
        string = input("Enter the string you want to compress:")
    elif h == 2:
        file = "data.txt"
        with open(file, 'r') as f:
            string = f.read()
            
    else:
        print("You entered invalid input")
    encoded = encode(string)
    decoded = decode(encoded)
    OutputFile= open('CompressedData.txt','w')
#    OutputFile2=open("Decompressed.txt",'w')
    OutputFile.write(formatOutput(encoded))
 #   OutputFile2.write(formatOutput(decoded))
    finish = time.perf_counter()
    t = str(round(finish-start, 3))
    print("Time to compress data = ", t , "seconds")
if __name__ == "__main__":
    main()
