import random
import time
def check(file):
    F = open(file,"r")
    status = F.readline()
    F.close
    if (status == "NMMY^J255Y[KQIQLJZQFV\n"):
        return True
    return False

def encrypt():
    file = 'CompressedData.txt'
    e = check(file)
    if (e):
        print("File already encrtpted")
    else:
        key = ""
        pas = ""
        fileE = "EncryptedData.txt"
        password = "Uzile" #input("Enter password:\n")
        for l in password: #Generate Key
            a = random.randint(0,5)
            ln = chr(ord(l)+a)
            pas = pas+ln
            key += chr(a)
        
        F = open(file,"r",encoding='utf-8')
        E = open(fileE, "w",encoding='utf-8')
        E.write("NMMY^J255Y[KQIQLJZQFV\n")
        E.write(key)
        E.write("\n")
        E.write(pas)
        E.write("\n")
        data = F.readlines()
        out = []
        for line in data: #Encrypt the Data
            i = 0
            newline =""
            for l in line:
                u = ord(key[i])
                i = i+1
                newline += chr(ord(l)+u)
                if (i == len(key)):
                    i = 0
            out.append(newline+"\n")
        E.writelines(out)
        E.close()
        F.close()

def main():
    start = time.perf_counter()
    encrypt()
    finish = time.perf_counter()
    t = str(round(finish-start,3))
    print("Time to encrypt is = ",t,"seconds" )

if __name__ == "__main__":
    main()
