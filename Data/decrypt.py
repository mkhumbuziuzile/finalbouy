def check(file):
    F = open(file,"r")
    status = F.readline()
    F.close
    if (status == "NMMY^J255Y[KQIQLJZQFV\n"):
        return True
    return False

def dencrypt():
    file = "EncryptedData.txt"
    e = check(file)
    if (e):
        F = open(file, "r", encoding='utf-8')
        D = open("DataUnlocked.txt", "w", encoding='utf-8')
        data = F.readlines()
        data.pop(0)
        for i in range(0,len(data)):
            data[i] = data[i][0:-1]
        key = data.pop(0)
        pa = data.pop(0)
        pas =""
        for i in range(0,len(pa)):
            u = ord(key[i])
            pas += chr(ord(pa[i])-u)

        p = input("Enter Password:\n    ")
        if (pas != p):
            print("Password does not match")
            D.write("Incorrect Password, decryption failed")
            D.close()
            F.close( )
            exit(-1)
        out = []
        for line in data:
            if (line!=''):
                i = 0
                Uline = ""
                for l in line:
                    u = ord(key[i])
                    i = i + 1
                    Uline += chr(ord(l)-u)
                    if (i==len(key)):
                        i = 0
                out.append(Uline)
        for i in range(0,len(out)-1):
            if (out[i][-2:].find('\n')==-1):
                out[i] = out[i]+"\n"
        D.writelines(out)
        D.close()
        F.close()

    else:
        print("File not encrypted using encryption method designed for this decryption")
        exit(-1)

def main():
    dencrypt()

if __name__ == "__main__":
    main()